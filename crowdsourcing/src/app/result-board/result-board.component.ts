import { AfterViewChecked, AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription, interval } from 'rxjs';
import { Idea } from '../models/idea.model';
import { Moyenne } from '../models/moyenne.model';
import { Session } from '../models/session.model';
import { IdeasServicesService } from '../services/ideas-services/ideas-services.service';
import { MoyennesServicesService } from '../services/moyennes-services/moyennes-services.service';
import { GetDataSessionService } from '../services/session-services/getDataSession/get-data-session.service';
const pdfMake  = require('pdfmake/build/pdfmake.js');
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
(<any>pdfMake).vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-result-board',
  templateUrl: './result-board.component.html',
  styleUrls: ['./result-board.component.css']
})

export class ResultBoardComponent implements OnInit {

  ideas!: Moyenne[];
  session!: Session[];
  ideasSubscription!: Subscription;
  idea!: Moyenne[];
  // data!: Idea;
  public idea_titre: any;
  public idea_desc: any;
  public moyenne_note:any;


  sessionID: any;

  constructor(private ideaService: IdeasServicesService, private dataSessionService: GetDataSessionService,
    private moyenneService: MoyennesServicesService, private router: Router) { }

  ngOnInit(): void {
    this.sessionID = (sessionStorage.getItem('sessionID'));

    // this.resultIdea();
    this.resultIdeaMoyenne();
    this.getSessionInfos();
    //this.ideaService.emitIdeas();
    //setInterval(() => {this.resultIdea()}, 3000);
  }


  /* resultIdea():void{
     this.ideaService.getIdeas().subscribe(
       (getIdeas: Idea[]) => {
       this.ideas = getIdeas;
       console.log(getIdeas)
     })
   }*/

  resultIdeaMoyenne(): void {
    this.moyenneService.getMoyennes(this.sessionID).subscribe(
      (getIdeas: Moyenne[]) => {
        this.ideas = getIdeas;
        console.log(getIdeas);
      }
    )
  }

  getSessionInfos() {
    let sessionName = String(sessionStorage.getItem('sessionName'));
    this.dataSessionService.getSessionFromServer(sessionName).subscribe(
      (getSession: Session[]) => {
        this.session = getSession;
      }
    );
  }

  seeMore(moyenne_id:number){
    var index = this.ideas.findIndex(x=>x.moyenne_id===moyenne_id)
    this.idea_titre=this.ideas[index].idea_titre;
    this.idea_desc=this.ideas[index].idea_desc;
    this.moyenne_note=this.ideas[index].moyenne_note;
  }

  destroySession() {
    sessionStorage.clear();
    this.router.navigate(['connexion'], { skipLocationChange: true });
  }

  openPDF(){
    const document = this.moyenneService.getDocument(this.ideas);
    pdfMake.createPdf(document).open();
  }

  /*ngOnDestroy(): void {
    this.ideasSubscription.unsubscribe();
  }*/
}
