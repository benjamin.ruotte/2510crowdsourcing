import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subscription, of } from 'rxjs';
import { Idea } from '../models/idea.model';
import { Note } from '../models/note.model';
import { Session } from '../models/session.model';
import { IdeasServicesService } from '../services/ideas-services/ideas-services.service';
import { NotesServicesService } from '../services/notes-services/notes-services.service';
import { MoyennesServicesService } from '../services/moyennes-services/moyennes-services.service';
import { Moyenne } from '../models/moyenne.model';
import { GetDataSessionService } from '../services/session-services/getDataSession/get-data-session.service';
import { PhasesServicesService } from '../services/phases-services/phases-services.service';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})

export class BoardComponent implements OnInit {

  idea: Idea[] = [];
  session!: Session[];
  data!: Idea;
  moyenne!: Moyenne;
  addNewIdeaForm!: FormGroup;
  public idea_titre: any;
  public idea_desc: any;
  isResultButtonVisible!: boolean;
  isCreateCardButtonVisible!: boolean;
  showButton!: boolean;
  public idea_id: any;
  public idea_note: any;
  userID: any;
  sessionID: any;
  addNoteForm!: FormGroup;
  timer: number = 2000;
  etape!: number;
  public resultShow: any;
  public evalShow: any;
  public ideaShow: any;
  public hiddenBoardLoaderStatus: any;

  interval: any;

  constructor(private formBuilder: FormBuilder, private ideaService: IdeasServicesService, private moyenneService: MoyennesServicesService, private dataSessionService: GetDataSessionService,
    private noteService: NotesServicesService, private router: Router, private ideasService: IdeasServicesService, private phasesService: PhasesServicesService) { }

  ngOnInit(): void {
    this.initCreateNewIdeaForm();
    this.getSessionInfos();
    this.userID = sessionStorage.getItem('userID');
    console.log("user " + this.userID);
    this.sessionID = sessionStorage.getItem('sessionID');
    console.log("session " + this.sessionID);
    this.etape = 2;
    this.resultShow = true;
    this.evalShow = true;
    this.ideaShow = false;
    this.hiddenBoardLoaderStatus = true;
  }

  ngOnDestroy(): void { }

  getSessionInfos() {
    let sessionName = String(sessionStorage.getItem('sessionName'));
    this.dataSessionService.getSessionFromServer(sessionName).subscribe(
      (getSession: Session[]) => {
        this.session = getSession;
      }
    );
  }

  /**
   * Permet de générer et de vérifier les champs du formulaire de création d'idée
   */
  initCreateNewIdeaForm() {
    this.addNewIdeaForm = this.formBuilder.group({
      idea_titre: this.formBuilder.control("", [Validators.required, Validators.minLength(3)]),
      idea_desc: this.formBuilder.control("", [Validators.required, Validators.minLength(10)])
    });
  }

  /**
   * Fonction de validation du formulaire de création d'idée
   */
  onSubmit(): void {
    const dataNewIdea = this.addNewIdeaForm.value;
    dataNewIdea.user_id = this.userID;
    const newIdea = new Idea(dataNewIdea.idea_id, dataNewIdea.idea_titre, dataNewIdea.idea_desc, dataNewIdea.user_id);
    this.ideaService.addNewIdea(newIdea);
    this.ideaShow = true;
    this.interval = setInterval(() => { this.listenPhase(this.sessionID) }, 3000);
  }

  listenPhase(sessionID: string) {
    this.hiddenBoardLoaderStatus = false;
    this.phasesService.getReadPhase(sessionID).subscribe(
      res => {
        if ((res != null) && (res[0].phase_numero == this.etape)) {
          if (res[0].type_phase_id == 2) {
            clearInterval(this.interval);
            this.hiddenBoardLoaderStatus = true;
            this.evalShow = false;
            this.etape += 1;
          } else if (res[0].type_phase_id == 3) {
            clearInterval(this.interval);
            this.hiddenBoardLoaderStatus = true;
            setTimeout(() => {
              this.resultShow = false;
            }, this.timer)
          }
          // clearInterval(this.interval);
        }
      }
    );
  }

  /**
   * Permet de récupérer une idée aléatoire dans la bdd
   */
  getRandomIdea() {
    // this.ideaService.getRandomIdeaFromServer().subscribe(res=>this.flag=res[0].idea_id);
    this.ideaService.getRandomIdeaFromServer(this.userID, this.sessionID).subscribe(res => {
      this.data = res[0];
      this.idea_id = this.data.idea_id;
      this.idea_titre = this.data.idea_titre;
      this.idea_desc = this.data.idea_desc;
    });
  }

  noteSubmit(idea_note: string): void {
    const NewNote = new Note(idea_note, this.userID, this.idea_id);
    this.noteService.addNewNote(NewNote);
    this.evalShow = true;
    this.interval = setInterval(() => { this.listenPhase(this.sessionID) }, 3000);
  }

  getResultat() {
    this.router.navigate(["resultats"], { skipLocationChange: true });
  }
}
