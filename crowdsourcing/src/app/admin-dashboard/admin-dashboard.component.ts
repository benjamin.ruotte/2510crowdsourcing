import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Phase } from '../models/phase.model';
import { Session } from '../models/session.model';
import { MoyennesServicesService } from '../services/moyennes-services/moyennes-services.service';
import { PhasesServicesService } from '../services/phases-services/phases-services.service';
import { GetDataSessionService } from '../services/session-services/getDataSession/get-data-session.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {

  userID: any;
  sessionID: any;
  session!: Session[];
  timer: number = 2000;
  etape!:number;
  timer2: number = 500;
  public nbEval:any;
  public phase1Show:any;
  public phase2Show:any;
  public phase3Show:any;

  constructor(private router: Router, private moyenneService: MoyennesServicesService, private dataSessionService: GetDataSessionService
    ,private phaseService: PhasesServicesService) { }

  ngOnInit(): void {
    this.etape=1;
    this.nbEval=0;
    this.userID = sessionStorage.getItem('userID');
    this.sessionID = sessionStorage.getItem('sessionID');
    console.log(this.sessionID);
    this.getSessionInfos();
    this.phase3Show=true;
    this.phase2Show=true;
    this.phase1Show=false;

  }

  async getSessionInfos() {

    setTimeout(async () => {
      let sessionName = String(sessionStorage.getItem('sessionName'));
      await this.dataSessionService.getSessionFromServer(sessionName).subscribe(
        (getSession: Session[]) => {
          this.session = getSession;
        }
      );
    }, this.timer2);
  }

  startPhase1(){
    sessionStorage.setItem('wait','0');
    const newPhase = new Phase(this.etape, 1,this.sessionID );
    this.phaseService.addNewPhase(newPhase);
    this.etape+=1;
    this.phase2Show=false;
    this.phase1Show=true;
  }

  startPhase2(){
    const newPhase = new Phase(this.etape, 2,this.sessionID );
    this.phaseService.addNewPhase(newPhase);
    this.etape+=1;
    this.nbEval+=1;
    this.phase3Show=false;

  }

  startPhase3(){
    this.phase2Show=true;
    this.phase3Show=true;
    this.getResultat();
    const newPhase = new Phase(this.etape, 3,this.sessionID );
    this.phaseService.addNewPhase(newPhase);


  }

  getResultat() {
    this.moyenneService.getCalculMoyenne(this.sessionID).subscribe();
    setTimeout(() => {
      this.router.navigate(["resultats"], { skipLocationChange: true });
    }, this.timer)
  }
}
