import { TestBed } from '@angular/core/testing';

import { IdeasServicesService } from './ideas-services.service';

describe('IdeasServicesService', () => {
  let service: IdeasServicesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IdeasServicesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
