import { TestBed } from '@angular/core/testing';

import { MoyennesServicesService } from './moyennes-services.service';

describe('NotesServicesService', () => {
  let service: MoyennesServicesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MoyennesServicesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
