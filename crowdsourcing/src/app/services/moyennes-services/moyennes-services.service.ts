import { formatDate } from '@angular/common';
import { HttpClient, HttpHeaders, HttpClientModule, HttpParams } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Observable, Subject, from } from 'rxjs';
import { map } from 'rxjs/operators';

import { Moyenne } from 'src/app/models/moyenne.model';



@Injectable({
  providedIn: 'root'
})

export class MoyennesServicesService {

  moyennes: Moyenne[] = [];
  moyennesSub = new Subject<Moyenne[]>();
  moyenne = [];
  body: any;
  sessionID: any;
  ideasTest2: any[] = [];

  constructor(private httpClient: HttpClient, private injector:Injector) { 
  }

  urlAPI=this.injector.get('URL');

  getMoyennes(session_id: number): Observable<Moyenne[]> {
    let parametres = new HttpParams();
    parametres = parametres.append('moyenne_id', session_id);
    return this.httpClient.get<Moyenne[]>("http://localhost:8001/api/moyenne/read.php?" + parametres.toString());
  }

  getCalculMoyenne(session_id:number): Observable<Moyenne[]>{
    let parametres = new HttpParams();
    parametres = parametres.append('moyenne_id', session_id);
   return this.httpClient.get<Moyenne[]>("http://localhost:8001/api/moyenne/moyennecalcul.php?" + parametres.toString()); 
  }

  getDocument(ideas: Moyenne[]) {

    for (let idea of Object.keys(ideas)) {
      const result: string[] = [];
      var ideasTest = { titre: [ideas[idea as any].idea_titre], desc: [ideas[idea as any].idea_desc], note: [ideas[idea as any].moyenne_note]};
      this.ideasTest2.push(ideasTest);
    }

    return {
      content: [
        { text: '25/10 Crowdsourcing', style: 'subheader' },
        ' ',
        { text: 'Date de la réunion : ' + formatDate(new Date(), 'dd/MM/yyyy', 'en'), style: 'subsubheader' },
        {
          stack: [
            sessionStorage.getItem('sessionObjet'),
          ],
          style: 'header',
        },
        ' ',
        {
          text: 'Les 10 meilleures idées de la session : ',
          style: 'subheader'
        },
        ' ',
        {
          ol: this.ideasTest2.map(function (item) {
            return { 
              stack:[
               { text: '\' ' + item.titre + ' \'', style: 'titre' },
               { text : '(Moyenne : ' + item.note + '/5) \n ', style:'note'},
               { text: 'Description : ' + item.desc + '\n\n'},
            ]
          }
            
          })
        },
        {
          text: '\n\nBravo pour ce brainstorming ! Nous vous souhaitons bon courage pour la mise en oeuvre de ces brillantes idées ! ;)', alignment: 'center',

        }
      ],
      styles: {
        defaultStyle: {
        },
        header: {
          fontSize: 18,
          bold: true,
          alignment: 'right',
          margin: [0, 80, 0, 80],
        },
        subheader: {
          fontSize: 14,
          bold: true,
          alignment: 'left',
        },
        subsubheader: {
          fontSize: 8,
          bold: true,
          alignment: 'left',
        },
        titre: {
          fontSize: 12,
          bold: true,
        },
        note: {
          fontSize: 10,
        },
      }
    }
  }
}



