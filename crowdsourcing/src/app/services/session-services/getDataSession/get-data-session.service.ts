import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Session } from 'src/app/models/session.model';

@Injectable({
  providedIn: 'root'
})
export class GetDataSessionService {

  constructor(private httpClient: HttpClient) { }

  getSessionFromServer(session_name: string): Observable<Session[]> {
    let idSession = new HttpParams().set('session_id', session_name);
    return this.httpClient.get<Session[]>("http://localhost:8001/api/session/readOnebyName.php?" + idSession.toString());
  }
}
