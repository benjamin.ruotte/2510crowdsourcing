import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { Session } from 'src/app/models/session.model';
import { NewSession } from 'src/app/models/newsession.model';

@Injectable({
  providedIn: 'root'
})
export class CreateSessionService {

  sessions: Session[] = [];
  newsessions: NewSession[] = [];
  constructor(private httpClient: HttpClient, private injector: Injector) { }

  urlAPI = this.injector.get('URL');

  async addNewSession(newsession: NewSession) {
    await this.newsessions.push(newsession);
    await this.saveNewSessionOnServer(newsession);
  }


  saveNewSessionOnServer(newsession: NewSession) {
    this.httpClient.post("http://localhost:8001/api/session/create.php", JSON.stringify(newsession)).subscribe(
      () => {
        console.log("La session a bien été créée.");
      },
      (error) => {
        console.log("Erreur " + JSON.stringify(error) + ". La session n'a pas pu être créée.");
      }
    )
  }

  getReadSession(): Observable<Session[]> {
    return this.httpClient.get<Session[]>("http://localhost:8001/api/session/read.php");
  }

  getReadSessionbyName(session_name: string): Observable<Session[]> {
    let parametres = new HttpParams().set('session_id', session_name);
    return this.httpClient.get<Session[]>("http://localhost:8001/api/session/readOnebyName.php?" + parametres.toString());
  }

  createRandomSessionName() {
    let sessionName = "";
    let sample = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 8; i++) {
      sessionName += sample.charAt(Math.floor(Math.random() * sample.length));
    }
    return sessionName;
  }
}
