import { TestBed } from '@angular/core/testing';

import { PhasesServicesService } from './phases-services.service';

describe('PhasesServicesService', () => {
  let service: PhasesServicesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PhasesServicesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
