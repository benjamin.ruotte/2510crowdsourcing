import { HttpClient, HttpHeaders, HttpClientModule, HttpParams } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { inject } from '@angular/core/testing';
import { Observable, Subject, from } from 'rxjs';

import { Phase } from 'src/app/models/phase.model';



@Injectable({
  providedIn: 'root'
})

export class PhasesServicesService {

  phases: Phase[] = [];
  phasesSub = new Subject<Phase[]>();
  phase = [];

  constructor(private httpClient: HttpClient, private injector: Injector) {
  }

  addNewPhase(phase: Phase): void {
    this.phases.push(phase);
    //this.emitIdeas();
    this.saveNewPhaseOnServer(phase);
  }


  saveNewPhaseOnServer(phase: Phase): void {
    this.httpClient.post("http://localhost:8001/api/phase/create.php", JSON.stringify(phase)).subscribe(
      () => {
        console.log("La phase a bien été enregistrée.");
      },
      (error) => {
        console.log("Erreur " + JSON.stringify(error) + ". La phase n'a pas pu être enregistrée.");
      }
    );
  }

  getReadPhase(session_id: string): Observable<Phase[]> {
    let parametres = new HttpParams().set('phase_id', session_id);
    return this.httpClient.get<Phase[]>("http://localhost:8001/api/phase/read.php?" + parametres.toString());
  }
}
