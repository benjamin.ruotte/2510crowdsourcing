<?php


if($_SERVER['REQUEST_METHOD'] == 'GET'){

    include_once '../config/database.php';
    include_once '../objects/user.php';

    // On instancie la base de données
    $database = new Database();
    $db = $database->getConnection();

    // On instancie les produits
    $user = new User($db);

      // On set l'id de l'enregistrement à lire
      $user->id = isset($_GET['user_id']) ? $_GET['user_id'] : die();


    // On récupère les données
    $stmt = $user->read();

    // On vérifie si on a au moins 1 produit
    if($stmt->rowCount() > 0){
        // On initialise un tableau associatif
        $users_arr = [];

        // On parcourt les produits
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $user = [
                "user_id" => $row['user_id'],
                "user_pseudo" => $row['user_pseudo'],
                "user_mail" => $row['user_mail'],
                "session_id" => $row['session_id']
            ];

            $users_arr[] = $user;
        }
        // On envoie le code réponse 200 OK
        http_response_code(200);

        // On encode en json et on envoie
        echo json_encode($users_arr);
    }
}else{
    http_response_code(405);
    echo json_encode(["message" => "La méthode n'est pas autorisée"]);
}
