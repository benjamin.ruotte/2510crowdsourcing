<?php


// On vérifie la méthode
if($_SERVER['REQUEST_METHOD'] === 'POST'){
    // On inclut les fichiers de configuration et d'accès aux données
    include_once '../config/database.php';
    include_once '../objects/session.php';

    // On instancie la base de données
    $database = new Database();
    $db = $database->getConnection();

    // On instancie les produits
    $session = new Session($db);

    // On récupère les informations envoyées
    $data = json_decode(file_get_contents("php://input"));
    if(!empty($data->session_name) && !empty($data->session_objet)){
        // Ici on a reçu les données
        // On hydrate notre objet
        $session->name = $data->session_name;
        $session->objet = $data->session_objet;
        if($session->create()){
            // Ici la création a fonctionné
            // On envoie un code 201
            http_response_code(201);
            echo json_encode(["message" => "L'ajout a été effectué"]);
        }else{
            // Ici la création n'a pas fonctionné
            // On envoie un code 503
            http_response_code(503);
            echo json_encode(["message" => "L'ajout n'a pas été effectué"]);         
        }
    }else{
        // Ici les données sont incomplètes
        // On envoie un code 400
        http_response_code(400);
        echo json_encode(["message" => "L'ajout n'a pas été effectué, il manque des données"]);
    }
}else{
    // On gère l'erreur
    http_response_code(405);
    echo json_encode(["message" => "La méthode n'est pas autorisée"]);
}