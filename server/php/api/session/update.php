<?php


// On vérifie la méthode
if($_SERVER['REQUEST_METHOD'] == 'PUT'){
    // On inclut les fichiers de configuration et d'accès aux données
    include_once '../config/database.php';
    include_once '../objects/session.php';

    // On instancie la base de données
    $database = new Database();
    $db = $database->getConnection();

    // On instancie les produits
    $session = new Session($db);

    // On récupère les informations envoyées
    $data = json_decode(file_get_contents("php://input"));
    
    // On set l'id de l'utilisateur à modifier
    $session->id = $data->session_id;

    // On hydrate l'objet
    $session->objet = $data->session_objet;
    $session->name = $data->session_name;
    $session->date_debut = $data->session_date_debut;
    $session->date_fin = $data->session_date_fin;

    if($session->update()){
        // Ici la modification a fonctionné
        // On envoie un code 200
        http_response_code(200);
        echo json_encode(["message" => "La modification a été effectuée"]);
    }else{
        // Ici la création n'a pas fonctionné
        // On envoie un code 503
        http_response_code(503);
        echo json_encode(["message" => "La modification n'a pas été effectuée"]);         
    }
}else{
    // On gère l'erreur
    http_response_code(405);
    echo json_encode(["message" => "La méthode n'est pas autorisée"]);
}