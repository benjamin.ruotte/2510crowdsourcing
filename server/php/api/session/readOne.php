<?php


// On vérifie que la méthode utilisée est correcte
if($_SERVER['REQUEST_METHOD'] == 'GET'){
    // On inclut les fichiers de configuration et d'accès aux données
    include_once '../config/database.php';
    include_once '../objects/session.php';

    // On instancie la base de données
    $database = new Database();
    $db = $database->getConnection();

    // On instancie les produits
    $session = new Session($db);

    // On set l'id de l'enregistrement à lire
    $session->id = isset($_GET['session_id']) ? $_GET['session_id'] : die();

     // On initialise un tableau associatif
     $sessions_arr = [];
    // On récupère le produit
    $session->readOne();
    if($session->objet != null){
        $session_arr = [
            "session_id" => $session->id,
            "session_name" => $session->name,
            "session_objet" => $session->objet,
            "session_date_debut" => $session->date_debut,
            "session_date_fin" => $session->date_fin,
        ];
    
        // On envoie le code réponse 200 OK
        http_response_code(200);

        // On encode en json et on envoie
        echo json_encode("[". $session_arr . "]");
    }else{
        // 404 Not found
        http_response_code(404);
         
        echo json_encode(array("message" => "L'utilisateur n'existe pas."));
    }  
}else{
    // On gère l'erreur
    http_response_code(405);
    echo json_encode(["message" => "La méthode n'est pas autorisée"]);
}