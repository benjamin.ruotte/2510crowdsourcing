<?php class User{
    private $connexion;
    private $table = "t_user";

    public $id;
    public $pseudo;
    public $mail;
    public $session_id;

    public function __construct($db){
        $this->connexion = $db;
    }

    public function read(){
        $sql = "SELECT user_id, user_pseudo, user_mail, session_id FROM " . $this->table." where session_id= ?";
        $query = $this->connexion->prepare($sql);
        $query->bindParam(1, $this->id);
        $query->execute();
        return $query;
    }

    public function create(){
        $sql = "INSERT INTO " . $this->table . " SET user_pseudo=:pseudo, user_mail=:mail, session_id=:session_id";
        $query = $this->connexion->prepare($sql);
        var_dump($this);
        $this->pseudo=htmlspecialchars(strip_tags($this->pseudo));
        $this->mail=htmlspecialchars(strip_tags($this->mail));
        $this->session_id=htmlspecialchars(strip_tags($this->session_id));
        $query->bindParam(":pseudo", $this->pseudo);
        $query->bindParam(":mail", $this->mail);
        $query->bindParam(":session_id", $this->session_id);
        var_dump($query);
        if($query->execute()){
            return true;
        }
        return false;
    }

    public function readOne(){
        // On écrit la requête
        $sql = "SELECT user_id, user_pseudo, user_mail, session_id FROM " . $this->table . " WHERE user_id = ? LIMIT 0,1";
        // On prépare la requête
        $query = $this->connexion->prepare( $sql );
        // On attache l'id
        $query->bindParam(1, $this->id);
        // On exécute la requête
        $query->execute();
        // on récupère la ligne
        $row = $query->fetch(PDO::FETCH_ASSOC);
        // On hydrate l'objet
        $this->id = $row['user_id'];
        $this->pseudo = $row['user_pseudo'];
        $this->mail = $row['user_mail'];
        $this->session_id = $row['session_id'];
    }

    public function readOnebyName(){
        // On écrit la requête
        $sql = "SELECT user_id, user_pseudo, user_mail, session_id FROM " . $this->table . " WHERE user_pseudo= ? LIMIT 0,1";
        // On prépare la requête
        $query = $this->connexion->prepare( $sql );
        // On attache l'id
        $query->bindParam(1, $this->id);
        // On exécute la requête
        $query->execute();
        // on récupère la ligne
        $row = $query->fetch(PDO::FETCH_ASSOC);
        // On hydrate l'objet
        $this->id = $row['user_id'];
        $this->pseudo = $row['user_pseudo'];
        $this->mail = $row['user_mail'];
        $this->session_id = $row['session_id'];
    }

    public function readLastEntry()
    {
        $sql = "SELECT user_id, user_pseudo, user_mail, session_id FROM " . $this->table . " ORDER BY user_id DESC LIMIT 1";
        $query = $this->connexion->prepare($sql);
        $query->execute();
        return $query;
    }

    public function update(){
        $sql = "UPDATE " . $this->table . " SET user_pseudo=:pseudo, user_mail=:mail WHERE user_id = :id";
        $query = $this->connexion->prepare($sql);
        $this->id=htmlspecialchars(strip_tags($this->id));
        $this->pseudo=htmlspecialchars(strip_tags($this->pseudo));
        $this->mail=htmlspecialchars(strip_tags($this->mail));
        $query->bindParam(":id", $this->id);
        $query->bindParam(":pseudo", $this->pseudo);
        $query->bindParam(":mail", $this->mail);
        if($query->execute()){
            return true;
        }
        return false;
    }

    public function delete(){
        $sql = "DELETE FROM " . $this->table . " WHERE user_id = ?";
        $query = $this->connexion->prepare($sql);
        $this->id=htmlspecialchars(strip_tags($this->id));
        $query->bindParam(1, $this->id);
        if($query->execute()){
            return true;
        }
        return false;
    }
}