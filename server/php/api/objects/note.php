<?php class Note{
    private $connexion;
    private $table = "t_note";

    public $id;
    public $note;
    public $user_id;
    public $idea_id;

    public function __construct($db){
        $this->connexion = $db;
    }

    public function read(){
        $sql = "SELECT note_id, note_note, user_id, idea_id FROM " . $this->table;
        $query = $this->connexion->prepare($sql);
        $query->execute();
        return $query;
    }

    public function create(){
        $sql = "INSERT INTO " . $this->table . " SET note_note=:note, user_id=:user_id, idea_id=:idea_id";
        $query = $this->connexion->prepare($sql);
        var_dump($this);
        $this->note=htmlspecialchars(strip_tags($this->note));
        $this->user_id=htmlspecialchars(strip_tags($this->user_id));
        $this->idea_id=htmlspecialchars(strip_tags($this->idea_id));
        $query->bindParam(":note", $this->note);
        $query->bindParam(":user_id", $this->user_id);
        $query->bindParam(":idea_id", $this->idea_id);
        var_dump($query);
        if($query->execute()){
            return true;
        }
        return false;
    }

    /*
    public function readOne(){
        // On écrit la requête
        $sql = "SELECT idea_id, idea_titre, idea_desc, session_id FROM " . $this->table . " WHERE idea_id = ? LIMIT 0,1";

        // On prépare la requête
        $query = $this->connexion->prepare( $sql );

        // On attache l'id
        $query->bindParam(1, $this->id);

        // On exécute la requête
        $query->execute();

        // on récupère la ligne
        $row = $query->fetch(PDO::FETCH_ASSOC);

        // On hydrate l'objet
        $this->id = $row['idea_id'];
        $this->titre = $row['idea_titre'];
        $this->desc = $row['idea_desc'];
        $this->session_id = $row['session_id'];
    }

    public function update(){
        $sql = "UPDATE " . $this->table . " SET idea_titre=:titre, idea_desc=:desc WHERE idea_id = :id";
        $query = $this->connexion->prepare($sql);
        $this->id=htmlspecialchars(strip_tags($this->id));
        $this->titre=htmlspecialchars(strip_tags($this->titre));
        $this->desc=htmlspecialchars(strip_tags($this->desc));
        $query->bindParam(":id", $this->id);
        $query->bindParam(":titre", $this->titre);
        $query->bindParam(":desc", $this->desc);
        if($query->execute()){
            return true;
        }
        return false;
    }

    public function delete(){
        $sql = "DELETE FROM " . $this->table . " WHERE idea_id = ?";
        $query = $this->connexion->prepare($sql);
        $this->id=htmlspecialchars(strip_tags($this->id));
        $query->bindParam(1, $this->id);
        if($query->execute()){
            return true;
        }
        return false;
    }*/
}