<?php class Phase{
    private $connexion;
    private $table = "t_phase";

    public $id;
    public $phase_numero;
    public $type_phase_id;
    public $session_id;

    public function __construct($db){
        $this->connexion = $db;
    }

    public function read(){
        $sql = "SELECT phase_numero, type_phase_id, session_id FROM " . $this->table ." where session_id=? order by phase_id desc limit 1";
        $query = $this->connexion->prepare($sql);
        $query->bindParam(1, $this->id);
        $query->execute();
        return $query;
    }

    public function create(){
        $sql = "INSERT INTO " . $this->table . " SET phase_numero=:phase_numero, type_phase_id=:type_phase_id, session_id=:session_id";
        $query = $this->connexion->prepare($sql);
        var_dump($this);
        $this->phase_numero=htmlspecialchars(strip_tags($this->phase_numero));
        $this->type_phase_id=htmlspecialchars(strip_tags($this->type_phase_id));
        $this->session_id=htmlspecialchars(strip_tags($this->session_id));
        $query->bindParam(":phase_numero", $this->phase_numero);
        $query->bindParam(":type_phase_id", $this->type_phase_id);
        $query->bindParam(":session_id", $this->session_id);
        var_dump($query);
        if($query->execute()){
            return true;
        }
        return false;
    }

    /*
    public function readOne(){
        // On écrit la requête
        $sql = "SELECT idea_id, idea_titre, idea_desc, session_id FROM " . $this->table . " WHERE idea_id = ? LIMIT 0,1";

        // On prépare la requête
        $query = $this->connexion->prepare( $sql );

        // On attache l'id
        $query->bindParam(1, $this->id);

        // On exécute la requête
        $query->execute();

        // on récupère la ligne
        $row = $query->fetch(PDO::FETCH_ASSOC);

        // On hydrate l'objet
        $this->id = $row['idea_id'];
        $this->titre = $row['idea_titre'];
        $this->desc = $row['idea_desc'];
        $this->session_id = $row['session_id'];
    }

    public function update(){
        $sql = "UPDATE " . $this->table . " SET idea_titre=:titre, idea_desc=:desc WHERE idea_id = :id";
        $query = $this->connexion->prepare($sql);
        $this->id=htmlspecialchars(strip_tags($this->id));
        $this->titre=htmlspecialchars(strip_tags($this->titre));
        $this->desc=htmlspecialchars(strip_tags($this->desc));
        $query->bindParam(":id", $this->id);
        $query->bindParam(":titre", $this->titre);
        $query->bindParam(":desc", $this->desc);
        if($query->execute()){
            return true;
        }
        return false;
    }

    public function delete(){
        $sql = "DELETE FROM " . $this->table . " WHERE idea_id = ?";
        $query = $this->connexion->prepare($sql);
        $this->id=htmlspecialchars(strip_tags($this->id));
        $query->bindParam(1, $this->id);
        if($query->execute()){
            return true;
        }
        return false;
    }*/
}