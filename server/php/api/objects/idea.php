<?php class Idea{
    private $connexion;
    private $table = "t_idea";

    public $id;
    public $titre;
    public $desc;
    public $user_id;
    public $session_id;

    public function __construct($db){
        $this->connexion = $db;
    }

    public function read(){
        $sql = "SELECT idea_id, idea_titre, idea_desc, user_id FROM " . $this->table ;
        $query = $this->connexion->prepare($sql);
        $query->execute();
        return $query;
    }

    public function readOneRandom(){
        $sql = "SELECT idea_id, idea_titre, idea_desc, user_id FROM " . $this->table ." ORDER BY RAND() LIMIT 1";
        $query = $this->connexion->prepare($sql);
        $query->execute();
        return $query;
    }

    public function create(){
        $sql = "INSERT INTO " . $this->table . " SET idea_titre=:titre, idea_desc=:desc, user_id=:user_id";
        $query = $this->connexion->prepare($sql);
        var_dump($this);
        $this->titre=htmlspecialchars(strip_tags($this->titre));
        $this->desc=htmlspecialchars(strip_tags($this->desc));
        $this->user_id=htmlspecialchars(strip_tags($this->user_id));
        $query->bindParam(":titre", $this->titre);
        $query->bindParam(":desc", $this->desc);
        $query->bindParam(":user_id", $this->user_id);
        var_dump($query);
        if($query->execute()){
            return true;
        }
        return false;
    }

    public function readOne(){
        // On écrit la requête
        $sql = "SELECT idea_id, idea_titre, idea_desc, user_id FROM " . $this->table . " WHERE idea_id = ? LIMIT 0,1";

        // On prépare la requête
        $query = $this->connexion->prepare( $sql );

        // On attache l'id
        $query->bindParam(1, $this->id);

        // On exécute la requête
        $query->execute();

        // on récupère la ligne
        $row = $query->fetch(PDO::FETCH_ASSOC);

        // On hydrate l'objet
        $this->id = $row['idea_id'];
        $this->titre = $row['idea_titre'];
        $this->desc = $row['idea_desc'];
        $this->user_id = $row['user_id'];
    }

    public function readOneRandomExcludID(){
        // On écrit la requête
        $sql = "SELECT ".$this->table.".idea_id, ".$this->table.".idea_titre, ".$this->table.".idea_desc, ".$this->table.".user_id FROM " . $this->table . " join t_user WHERE t_user.user_id=".$this->table.".user_id and t_idea.user_id!= ? and t_user.session_id= ? ORDER by rand()";

        // On prépare la requête
        $query = $this->connexion->prepare( $sql );

        // On attache l'id
        $query->bindParam(1, $this->user_id);
        $query->bindParam(2, $this->session_id);
        

        // On exécute la requête
        $query->execute();

        // on récupère la ligne
        $row = $query->fetch(PDO::FETCH_ASSOC);

        // On hydrate l'objet
        $this->id = $row['idea_id'];
        $this->titre = $row['idea_titre'];
        $this->desc = $row['idea_desc'];
        $this->user_id = $row['user_id'];
    }


    public function update(){
        $sql = "UPDATE " . $this->table . " SET idea_titre=:titre, idea_desc=:desc WHERE idea_id = :id";
        $query = $this->connexion->prepare($sql);
        $this->id=htmlspecialchars(strip_tags($this->id));
        $this->titre=htmlspecialchars(strip_tags($this->titre));
        $this->desc=htmlspecialchars(strip_tags($this->desc));
        $query->bindParam(":id", $this->id);
        $query->bindParam(":titre", $this->titre);
        $query->bindParam(":desc", $this->desc);
        if($query->execute()){
            return true;
        }
        return false;
    }

    public function delete(){
        $sql = "DELETE FROM " . $this->table . " WHERE idea_id = ?";
        $query = $this->connexion->prepare($sql);
        $this->id=htmlspecialchars(strip_tags($this->id));
        $query->bindParam(1, $this->id);
        if($query->execute()){
            return true;
        }
        return false;
    }
}