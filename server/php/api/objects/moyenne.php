<?php class Moyenne{
    private $connexion;
    private $table = "t_moyenne";

    public $id;
    public $titre;
    public $desc;
    public $note;

    public function __construct($db){
        $this->connexion = $db;
    }

    public function read(){
        $sql = "select t_idea.idea_id as moyenne_id, t_idea.idea_titre, t_idea.idea_desc, t_moyenne.moyenne_note  
        from t_idea left join t_moyenne on t_moyenne.idea_id=t_idea.idea_id join t_user 
        where t_idea.user_id=t_user.user_id and t_user.session_id= ? order by t_moyenne.moyenne_note desc limit 10";
        $query = $this->connexion->prepare($sql);
         // On attache l'id
        $query->bindParam(1, $this->id);
        $query->execute();
        return $query;
    }

    public function moyennecalcul(){
        $sql = "insert INTO t_moyenne (idea_id, moyenne_note) 
        SELECT t_note.idea_id,AVG(t_note.note_note)from t_note join t_user where 
        t_note.user_id=t_user.user_id and t_user.session_id= ? GROUP by idea_id;";
        $query = $this->connexion->prepare($sql);
         // On attache l'id
        $query->bindParam(1, $this->id);
        $query->execute();
        return $query;
    }
   
}