<?php


// On vérifie la méthode
if($_SERVER['REQUEST_METHOD'] == 'PUT'){
    // On inclut les fichiers de configuration et d'accès aux données
    include_once '../config/database.php';
    include_once '../objects/idea.php';

    // On instancie la base de données
    $database = new Database();
    $db = $database->getConnection();

    // On instancie les produits
    $idea = new Idea($db);

    // On récupère les informations envoyées
    $data = json_decode(file_get_contents("php://input"));
    
    // On set l'id de l'utilisateur à modifier
    $idea->id = $data->idea_id;

    // On hydrate l'objet
    $idea->titre = $data->idea_titre;
    $idea->desc = $data->idea_desc;

    if($idea->update()){
        // Ici la modification a fonctionné
        // On envoie un code 200
        http_response_code(200);
        echo json_encode(["message" => "La modification a été effectuée"]);
    }else{
        // Ici la création n'a pas fonctionné
        // On envoie un code 503
        http_response_code(503);
        echo json_encode(["message" => "La modification n'a pas été effectuée"]);         
    }
}else{
    // On gère l'erreur
    http_response_code(405);
    echo json_encode(["message" => "La méthode n'est pas autorisée"]);
}