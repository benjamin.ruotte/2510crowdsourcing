<?php


// On vérifie que la méthode utilisée est correcte
if($_SERVER['REQUEST_METHOD'] === 'DELETE'){
    // On inclut les fichiers de configuration et d'accès aux données
    include_once '../config/database.php';
    include_once '../objects/idea.php';

    // On instancie la base de données
    $database = new Database();
    $db = $database->getConnection();

    // On instancie les produits
    $idea = new Idea($db);

    // On récupère l'id du produit
    $data = json_decode(file_get_contents("php://input"));

    if(!empty($data->idea_id)){
        $idea->id = $data->idea_id;

        if($idea->delete()){
            // Ici la suppression a fonctionné
            // On envoie un code 200
            http_response_code(200);
            echo json_encode(["message" => "La suppression a été effectuée"]);
        }else{
            // Ici la création n'a pas fonctionné
            // On envoie un code 503
            http_response_code(503);
            echo json_encode(["message" => "La suppression n'a pas été effectuée"]);         
        }
    }
}else{
    // On gère l'erreur
    http_response_code(405);
    echo json_encode(["message" => "La méthode n'est pas autorisée"]);
}