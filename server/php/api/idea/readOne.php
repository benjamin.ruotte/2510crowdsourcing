<?php



// On vérifie que la méthode utilisée est correcte
if($_SERVER['REQUEST_METHOD'] == 'GET'){
    // On inclut les fichiers de configuration et d'accès aux données
    include_once '../config/database.php';
    include_once '../objects/idea.php';

    // On instancie la base de données
    $database = new Database();
    $db = $database->getConnection();

    // On instancie les produits
    $idea = new Idea($db);

    // On set l'id de l'enregistrement à lire
    $idea->id = isset($_GET['idea_id']) ? $_GET['idea_id'] : die();

    $ideas_arr_test = [];

    // On récupère le produit
    $idea->readOne();
    if($idea->titre != null){
        $idea_arr = [
            "idea_id" => $idea->id,
            "titre" => $idea->titre,
            "desc" => $idea->desc,
            "user_id" => $idea->user_id
        ];
        $ideas_arr_test[] = $idea_arr;
        // On envoie le code réponse 200 OK
        http_response_code(200);

        // On encode en json et on envoie
        echo json_encode($ideas_arr_test);
    }else{
        // 404 Not found
        http_response_code(404);
         
        echo json_encode(array("message" => "L'utilisateur n'existe pas."));
    }  
}else{
    // On gère l'erreur
    http_response_code(405);
    echo json_encode(["message" => "La méthode n'est pas autorisée"]);
}